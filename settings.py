import rulestrings as rs
import forms

## Globals
pixel = 6
X = 192
Y = 96

## GHL
PACE = 0.01

## World
# HARD = False
HARD = True
LIFESPAN = 24
INIT_MARGIN = X//16
TOROIDAL = True
INF_LOOP = False

## Conway's
RULESTRING = rs.Conway
KERNEL = [0, 1]

## HighLife
# RULESTRING = rs.HighLife


## Explode case:
# TOROIDAL = False
# KERNEL = [0, 0, 1.5] ## Explode

## Notable kernels
# KERNEL = [0, 0.1, 1.4] ## Pulsars rotacionals
# KERNEL = [0, 0.5, 1] ## Sopa
# KERNEL = [0, 0.6, 1] ## Cyberpunk
# KERNEL = [0.1, 0.6, 0.9] ## Frogs

# KERNEL = [0, 0, 1]
# KERNEL = [0, 0.136, 0.6828, 0.136] ## Gaussian


## TOP
LIFESPAN = 60
# RULESTRING = rs.Bugs #Bugs :: (H) Desert rose (Cyclic!) ; LIFESPAN = 24 o 60
# RULESTRING = rs.Coral #Coral (Explosiu/Regeneratiu) ;LIFESPAN = 60

# KERNEL = [0, 0, 0.75]
# KERNEL = [0, 0.5, 0.75]
# KERNEL = [0, 0.75, 0.25]
# KERNEL = [0, 1, 0]
# KERNEL = [0.25, 0.25, 1]
# KERNEL = [0.25, 1, 1]


## Registry
HSIZE = 2 * LIFESPAN


## Init form: Glider Gun
# init_form = forms.GliderGun
# pixel = 8
# X = 96
# Y = 48
# KERNEL = [0, 1]
# RULESTRING = rs.Conway
# INIT_MARGIN = 4
# LIFESPAN = 29 ## Periode de rebot
# TOROIDAL = False
# INF_LOOP = True
