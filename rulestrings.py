
Conway = "B3/S23"
HighLife = "B36/S23"

InverseLife = "B0123478/S34678"
Htrees = "B1/S012345678" #H-trees :: (H) Waved-Htrees
Life_wo_Death = "B36/S12345678" #Life without death, but with death
DotLife = "B36/S023" #DotLife - Dotdeath
Mazectric = "B3/S1234" #Mazectric generation
Bacteria = "B34/S456"
Amoeba = "B357/S1358"
Geology = "B3578/S24678"

## TOP
Coral = "B3/S45678" #Coral (Explosiu/Regeneratiu) ; LIFESPAN = 60
Bugs = "B3567/S15678" #Bugs :: (H) Desert rose (Cyclic!) ; LIFESPAN = 24
