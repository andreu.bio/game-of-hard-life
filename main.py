import copy
import os
import re
import time

import tkinter as tk
from PIL import Image, ImageTk

import yaml
from os import path, makedirs

import numpy as np
from numba import njit, prange

from settings import *


class Registry:
    id: str
    history: list
    life_history: list
    hsize: int = HSIZE
    alive: list
    reg: dict

    def __init__(self, world):
        shape = world.shape
        self.id = f'HL-{time.time()}'
        self.history = [np.zeros(shape)] * self.hsize
        self.life_history = [np.zeros(shape)] * self.hsize
        self.alive = list()
        self.reg = dict(
            shape=[world.shape[i] for i in range(len(world.shape))],
            lifespan=world.lifespan,
            toroidal=world.toroidal,
            inf_loop=world.infinity_loop,
            hsize=self.hsize,
            stats=dict()
        )

    def reg_hist(self, world):
        self.history[self.hist_pos(world.gen)] = copy.copy(world.cells)
        self.life_history[self.hist_pos(world.gen)] = copy.copy(world.lifecycle)
        self.reg_stats(stat=world.get_stats())

    def hist_pos(self, gen):
        return gen % self.hsize

    def reg_alive(self, alive):
        self.alive.append(alive)

    def get_stats(self):
        return self.reg

    def reg_stats(self, stat):
        ## dictionary to export as YAML
        self.reg['stats'][stat['gen']] = dict(
            gen=stat['gen'],
            kernel=stat['kernel'],
            rulestring=stat['rulestring'],
            alive=stat['alive'],
            dying=stat['dying'],
            cycle=stat['cycle'],
            ctime=stat['ctime'],
            uptime=stat['uptime']
        )

    def get_alive(self):
        return self.alive

    def save_stats(self):
        local_tests = os.path.join(os.path.curdir, "tests")
        if not path.exists(local_tests):
            makedirs(local_tests)

        with open(os.path.join(local_tests, self.id + ".yaml"), "w") as yaml_file:
            yaml.dump(self.get_stats(), yaml_file,
                      width=None, canonical=False)
            # , default_flow_style=True)
            yaml_file.close()


class World:
    gen: int
    cycle: int
    shape = tuple
    cells: np.ndarray
    updated_cells: np.ndarray
    lifecycle: np.ndarray
    lifespan: int = LIFESPAN
    registry: Registry
    toroidal: bool = True
    infinity_loop: bool = False
    kernel: np.ndarray
    birth: str
    survival: str
    dying: int = 0
    ctime: float = .0
    uptime: float = .0

    def __init__(self, Kernel=[0, 1], shape=(X, Y), rulestring=RULESTRING):
        self.gen, self.cycle = 0, 0
        self.shape = shape
        self.registry = Registry(self)
        ## WIP: improve self.ignite/randomize switch
        self.ignite(init_form) if 'init_form' in globals() else self.randomize()
        self.set_margin(INIT_MARGIN)
        self.toroidal = TOROIDAL
        self.infinity_loop = INF_LOOP
        self.registry.reg_alive(alive=self.get_alive())
        self.setKernel(Kernel)
        self.setRulestring(rulestring)

    def ignite(self, form):
        self.cells = np.zeros(self.shape)

        ## Center form into shape
        init_shape = form.shape
        shift = tuple(np.subtract(self.shape, init_shape) // 2)
        for row, col in np.ndindex(init_shape):
            self.cells[shift[0] + row, shift[1] + col] = form[row, col]

        self.lifecycle = self.cells * self.lifespan

    def randomize(self):
        self.cells = np.random.choice(a=[0, 1], size=self.shape, p=[0.72, 0.28]).astype(int)
        # self.cells = np.random.choice(a=[0, 1], size=self.shape, p=[0.82, 0.18])
        # self.cells = np.random.choice(a=[0, 1], size=self.shape, p=[0.92, 0.08])
        self.lifecycle = np.multiply(self.cells, self.lifespan)

    def zeros(self):
        self.cells = np.zeros(self.shape, dtype=int)
        self.lifecycle = np.multiply(self.cells * self.lifespan)

    def ones(self):
        self.cells = np.ones(self.shape, dtype=int)
        self.lifecycle = np.multiply(self.cells * self.lifespan)

    def inc_gen(self):
        self.gen += 1
        self.lifecycle -= self.cells

    def setRulestring(self, rulestr):
        birth, survival = re.findall(r'B(\d*)/S(\d*)', RULESTRING)[0]
        self.birth = birth
        self.survival = survival

    def getRulestring(self):
        return f'B{self.birth}/S{self.survival}'

    def setKernel(self, a=[0, 1]):
        ## Distance factor
        self.kernel = np.array(a)

    ## No usages » ker()
    def kernel_slice(self, row, col):
        ksize = self.kernel.shape[0]
        neighbours = 0
        for i, j in [(x, y) for x in range(-1, ksize) for y in range(-1, ksize)]:
            neighbours += np.multiply(
                self.cells[(row + i) % X, (col + j) % Y],
                self.kernel[np.maximum(np.abs(i), np.abs(j))]
            )

        if (self.cells[row, col] > 0 and 2 <= neighbours <= 3) \
                or (self.cells[row, col] == 0 and neighbours == 3 and self.respawn((row, col))):
            self.updated_cells[row, col] = 1 * ((HARD and (self.lifecycle[row, col] > 0)) or not HARD)

        return neighbours

    def update(self):
        timestamp_start = time.time()

        ## Update world elements
        self.inc_gen()
        self.cells = self.up_cell()
        self.cycle = self.check_cycles()

        self.uptime = time.time() - timestamp_start

        ## Add new historic registry
        self.registry.reg_hist(self)
        # self.registry.reg_alive(alive=self.get_alive())
        # self.registry.reg_stats(stat=self.get_stats())

        return self.get_stats()

    def respawn(self, pos):
        self.lifecycle[pos] = self.lifespan
        return True

    def check_cycles(self):
        pos = self.registry.hist_pos(gen=self.gen)
        for n in range(1, len(self.registry.history)):
            i = (pos - n) % self.registry.hsize  ## Check history regs backwards
            if (self.cells == self.registry.history[i]).all():
                return (pos - i) % self.registry.hsize  ## Modular distance in cyclic list

        return 0

    def get_alive(self):
        return int(self.cells.sum())

    def get_dying(self):
        return self.dying

    def get_stats(self):
        return dict(
            gen=self.gen,
            kernel=[float(self.kernel[i]) for i in range(self.kernel.shape[0])],
            rulestring=self.getRulestring(),
            cycle=self.cycle,
            alive=self.get_alive(),
            dying=self.get_dying(),
            ctime=self.ctime,
            uptime=self.uptime
        )

    def mask(self, mask=np.ones((X, Y), dtype=int)):
        self.cells = np.multiply(self.cells, mask)
        self.lifecycle = np.multiply(self.lifecycle, mask)

    def set_margin(self, margin: int):
        mask = np.ones(self.shape, dtype=int)
        for x, y in np.ndindex(self.shape):
            if not (((X - margin) > x > margin) and ((Y - margin) > y > margin)):
                mask[x, y] = 0

        self.mask(mask)

    def up_cell(self):

        timestamp_start = time.time()
        self.updated_cells = np.zeros(self.shape, dtype=int)

        for row, col in np.ndindex(self.shape):
            if self.toroidal:  ## TOROIDAL
                neighbours = ker(self.cells, self.kernel, row, col)

            else:  ## FLAT
                neighbours = np.sum(self.cells[row - 1:row + 2, col - 1:col + 2]) - self.cells[row, col]

            neigh = str(int(np.trunc(neighbours)))
            # if (self.cells[row, col] > 0 and 2 <= neighbours <= 3) \
            if (self.cells[row, col] > 0 and (neigh in self.survival)) \
                    or (self.cells[row, col] == 0 and (neigh in self.birth) and self.respawn((row, col))):
                self.updated_cells[row, col] = int(1 * ((HARD and (self.lifecycle[row, col] > 0)) or not HARD))

        self.dying = dying_cells(self.cells, self.updated_cells)
        self.ctime = time.time() - timestamp_start
        return self.updated_cells


@njit#(parallel=True)
def ker(cells, kernel, row, col):
    ksize = kernel.shape[0]
    neighbours = 0
    for i, j in [(x, y) for x in prange(-ksize + 1, ksize) for y in prange(-ksize + 1, ksize)]:
        neighbours += np.multiply(
            cells[(row + i) % X, (col + j) % Y],
            kernel[np.maximum(np.abs(i), np.abs(j))]
        )

    return neighbours


@njit(parallel=True)
def dying_cells(cells, updated_cells):
    return int(np.sum(np.greater(cells, updated_cells)))


class GameOfHardLife:
    world: World
    running: bool = False
    gui: tk.Tk
    frame: tk.Frame
    canvas: tk.Canvas
    image: ImageTk.PhotoImage
    panel: tk.Image
    info_bar: tk.Frame

    def __init__(self):
        self.world = World(Kernel=KERNEL)
        self.gui = tk.Tk()
        self.gui.title("Game of Hard Life")

        ## Key bindings
        self.gui.bind("<KeyPress>", self.keydown)
        self.gui.bind("<KeyRelease>", self.keyup)

        self.frame = tk.Frame(self.gui, width=X * pixel, height=Y * pixel)
        self.frame.pack()
        self.canvas = tk.Canvas(self.frame, width=(X + 2) * pixel, height=(Y + 2) * pixel)
        self.canvas.pack()

        self.info_bar = tk.Frame(self.gui)
        self.info_label = tk.Label(self.info_bar)
        self.info_bar.pack()
        self.info_label.pack()

        self.draw_world()
        self.show_bar(self.world.get_stats(), labels=True)
        self.gui.update()

        self.gui.after(0, self.run)
        self.gui.mainloop()

    def keydown(self, k):

        if k.keysym == 'space':
            self.one_step()
            self.world.registry.save_stats()
        elif (k.keysym == 'Return') \
                or (k.keysym == 'KP_Enter'):
            self.play_pause()

    def keyup(self, k):
        print(f'up {k}')
        return

    def show_bar(self, stats, labels=False):
        '''
        stats_keys = list(stats.keys())
        for j in range(len(stats_keys)):
            if labels:
                bar_label = tk.Label(self.info_bar, text=stats_keys[j])
                bar_label.grid(row=0, column=j, ipadx=6, ipady=6)
            bar_value = tk.Label(self.info_bar, text=stats[stats_keys[j]])
            bar_value.grid(row=1, column=j, ipadx=6, ipady=6)
       '''

        info = f'Generation: {stats["gen"]} | Alive: {stats["alive"]} | Dying: {stats["dying"]} | Cycle: {stats["cycle"]}'
        self.info_label.config(text=info)

    def draw_world(self):
        ## Dying cells
        buffer = np.uint8(255 * self.world.cells * (
                self.world.lifecycle + self.world.lifespan
        ) / (2 * self.world.lifespan)
                          ) if HARD else np.uint8(255 * self.world.cells)
        buffer = np.repeat(buffer, pixel, axis=0)
        buffer = np.repeat(buffer, pixel, axis=1)

        self.img = ImageTk.PhotoImage(image=Image.fromarray(buffer.transpose(), mode='L'))
        self.canvas.create_image(pixel, pixel, anchor="nw", image=self.img)

    def next_gen(self):
        stats = self.world.update()
        self.draw_world()
        self.show_bar(stats)
        return stats

    def play_pause(self):
        self.running = not self.running
        if self.running:
            self.gui.after(0, self.run)

    def one_step(self):
        self.running = False
        return self.step()

    def step(self):
        stats = self.next_gen()
        self.gui.update()
        return stats

    def run(self):
        running = self.running
        while running:
            stats = self.step()

            running = self.running \
                      and (stats["cycle"] == 0 \
                           or self.world.infinity_loop)
            # and (stats["gen"] < 1728)

            sleep = PACE - stats["uptime"]
            if (sleep > 0):
                ## print(f'{PACE} - ({time.time()} - {timestamp_start}): {sleep}')
                time.sleep(sleep)

        self.world.registry.save_stats()


if __name__ == '__main__':
    HL = GameOfHardLife()
