# Game of Hard Life

## Life is Hard, by definition

In this FDP it is intended analyse complex systems based on a basic implementation of the well-known *Game of life*, originally devised by the mathematician John Conway, to expand its conditions and create a variant where more complex situations can be considered, apply pattern recognition algorithms and a decision-making system in order to analyse the consequences of the resulting emergent systems.

## Use

### Run: main
Execute the main script to get a user's GUI:

```shell
python main.py
```

Press `Enter` to stop/pause the simulation.
Press `Space` to run a single generation.

### Run: agent

Or run the agent to lunch simulations without GUI:

```shell
python agent.py
```

### Configure

The `settings.py` manages the different configuration parameters to control the environment:

Setting           | Description
:--               | :--
**pixel, X, Y**   | Set the size of the *World* grid and pixels per cell.
**PACE**          | Minimal time to spend on each generation.
**HARD**          | *Game of Hard Life* mode. It activates cells lifespan.
**LIFESPAN**      | Number of max generations each cell can stay alive.
**INIT_MARGIN**   | Margin mask used on the initial setting of random cells over the grid.
**TOROIDAL**      | The grid behaves as the projection of a toroid's surface.
**HSIZE**         | Number of generations' grid saved in the historic *Registry*, used to determine cyclic patterns.
**INF_LOOP**      | Set whether the simulation must stop or keep running after finding a cyclic pattern (`False` by default).
**RULESTRING**    | Set the rule string which describes when each cell can be born or survive.
**KERNEL**        | Set the values of the Kernel vector used to determine how distant neighbours affect.

### Data

At the end of each Run, the simulation statistics are saved into the `tests` path in YAML format.
e.g.:

```yaml
hsize: 120
inf_loop: false
lifespan: 60
shape:
- 192
- 96
stats:
  1:
    alive: 2448
    ctime: 0.566856861114502
    cycle: 0
    dying: 2763
    gen: 1
    kernel:
    - 0.0
    - 1.0
    rulestring: B3/S45678
    uptime: 0.5687510967254639
  2:
    alive: 1668
    ctime: 0.04221653938293457
    cycle: 0
    dying: 2163
    gen: 2
    kernel:
    - 0.0
    - 1.0
    rulestring: B3/S45678
    uptime: 0.0440213680267334
## ... 
  1728:
    alive: 9175
    ctime: 0.05592179298400879
    cycle: 0
    dying: 1093
    gen: 1728
    kernel:
    - 0.0
    - 1.0
    rulestring: B3/S45678
    uptime: 0.05725383758544922
toroidal: true
```
