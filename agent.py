from main import World, ker, dying_cells
from settings import *
from numpy import linspace

## 7^3: 344 kernels
## (real    164m31.945s)
dimensions = 3
partitions = 7

kerr = [0, 1]
Kernels = [kerr]

from numba import njit, prange
@njit(parallel=True, nonpython=True)
def run_parallel(Kernels):
    for i in prange(len(Kernels)):
        print(Kernels[i])
        run(Kernels[i])

def run(kernel):
    world = World(Kernel=kernel)
    running = True
    while running:
        stats = world.update()  ## No GUI
        running = running \
                  and (stats["cycle"] == 0) \
                  and (stats["gen"] < 1728)
        #
        if (world.gen % 144 == 0):
            print(f'[{world.kernel}] Gen {stats["gen"]}: {stats["alive"]} | {stats["dying"]}')

    print(f'{stats["alive"]} | {stats["dying"]}')
    world.registry.save_stats()



# for dim in range(dimensions):
for alpha in linspace(0., 1, partitions):
    for betta in linspace(0., 1, partitions):
        for gamma in linspace(0., 1, partitions):
            Kernels.append([alpha, betta, gamma])


print(Kernels)
print(f'{len(Kernels)} kernels')

for k in Kernels:
    print(f' K e r n e l : {k}')
    run(k)
